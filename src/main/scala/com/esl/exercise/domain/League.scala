package com.esl.exercise.domain

case class League(leagueId: String, leagueType: String, participantsCount: Int, participants: List[ParticipantPosition])
