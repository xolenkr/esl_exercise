package com.esl.exercise.client

import com.esl.exercise.domain.{League, ParticipantPosition}
import com.twitter.util.{Await, Future}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class LeagueHttpClientIntegrationTest extends AnyWordSpec with Matchers {

  trait Fixtures {
    val limit: Int = 3
    val leagueClient: LeagueHttpClient = LeagueHttpClient()
    val teamLeagueId: String = "216337"
    val teamLeague: League = League(
      leagueId = teamLeagueId,
      leagueType = "team",
      participantsCount = 97,
      participants = List(
        ParticipantPosition("16286512", 1),
        ParticipantPosition("14484331", 2),
        ParticipantPosition("8421369", 3)
      )
    )

    val userLeagueId = "92347"
    val userLeague: League = League(
      leagueId = userLeagueId,
      leagueType = "user",
      participantsCount = 126,
      participants = List(
        ParticipantPosition("2992252", 1),
        ParticipantPosition("16235098", 2),
        ParticipantPosition("2502874", 3)
      )
    )
  }

  "getLeagueWithParticipants" should {
    "return league with participants for team league" in new Fixtures {
      val result: League = Await.result(leagueClient.getLeagueWithParticipants(teamLeagueId, limit))

      result shouldBe teamLeague
    }
    "return league with participants for user league" in new Fixtures {
      val result: League = Await.result(leagueClient.getLeagueWithParticipants(userLeagueId, limit))

      result shouldBe userLeague
    }
  }
}
