package com.esl.exercise.service

import com.esl.exercise.client.LeagueHttpClient
import com.esl.exercise.domain.Ranking
import com.twitter.util.Await
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class LeagueServiceIntegrationTest extends AnyWordSpec with Matchers {

  trait Fixtures {
    val leagueClient: LeagueHttpClient = LeagueHttpClient()
    val leagueService: LeagueService = new LeagueService(leagueClient)
    val leagueIds: List[String] = List ("90116", "92347", "134540")
    val limit: Int = 4
    val rankings: List[Ranking] = List(
      Ranking(1,"1", "6633867", 403.0),
      Ranking(2,"2", "9482972", 201.0),
      Ranking(3,"3", "3111615", 134.0),
      Ranking(4,"4", "2992252", 126.0)
    )
  }

  "getRankings" should {
    "get list of the rankings from the existing leagueIds" in new Fixtures {
      val result: List[Ranking] = Await.result(leagueService.getRankings(leagueIds, limit))

        result shouldBe rankings
    }

    "get empty list of the rankings when list of the leagueIds is empty" in new Fixtures {
      val result: List[Ranking] = Await.result(leagueService.getRankings(List.empty, limit))

        result shouldBe List.empty
    }
  }
}
