package com.esl.exercise.client

import com.esl.exercise.domain.League
import com.twitter.finagle.http.{Request, RequestBuilder, Response}
import com.twitter.finagle.{Http, Service}
import com.twitter.util.Future
import io.circe.parser._

class LeagueHttpClient(httpClient: Service[Request, Response]) extends LeagueClient {

  override def getLeagueWithParticipants(leagueId: String, limit: Int): Future[League] = {
    val query = Request.queryString(
      s"https://api.eslgaming.com/play/v1/leagues/$leagueId/ranking",
      ("limit", s"$limit"),
      ("offset", "0"))
    val request = RequestBuilder().url(query).buildGet()
    val decoder = new LeagueDecoder(leagueId)
    httpClient
      .apply(request)
      .map(response => {
        decode[League](response.contentString)(decoder).toTry.get
      })
  }
}

object LeagueHttpClient {

  def apply(): LeagueHttpClient = new LeagueHttpClient(
    Http.client
      .withTransport
      .tls
      .newService("api.eslgaming.com:443"))
}
