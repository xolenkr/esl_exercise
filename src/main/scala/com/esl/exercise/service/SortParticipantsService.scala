package com.esl.exercise.service

import com.esl.exercise.domain.{ParticipantPoints, Ranking}

object SortParticipantsService {

  def sort(participants: List[ParticipantPoints], limit: Int): List[Ranking] =
    participants
      .sortBy(-_.points)
      .take(limit)
      .zipWithIndex
      .map {
        case (participantPoints, index) => Ranking(
          position = index + 1,
          label = s"${index + 1}",
          participantId = participantPoints.participantId,
          points = participantPoints.points
        )
      }
}
