package com.esl.exercise.client

import com.esl.exercise.domain.League
import com.twitter.util.Future

trait LeagueClient {

  def getLeagueWithParticipants(leagueId: String, limit: Int): Future[League]
}
