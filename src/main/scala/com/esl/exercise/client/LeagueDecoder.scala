package com.esl.exercise.client

import com.esl.exercise.domain.{League, ParticipantPosition}
import io.circe.Decoder.Result
import io.circe._

class LeagueDecoder(leagueId: String) extends Decoder[League] {

  def decodeParticipantPosition(leagueType: String): Decoder[ParticipantPosition] = (c: HCursor) => for {
    id <- c.downField(leagueType).get[String]("id")
    position <- c.downField("position").as[Int]
  } yield ParticipantPosition(id, position)

  def decodeParticipantPositions(leagueType: String): Decoder[List[ParticipantPosition]] = Decoder.decodeList(decodeParticipantPosition(leagueType))

  override def apply(c: HCursor): Result[League] =
    for {
      leagueType <- c.downField("type").as[String]
      total <- c.downField("pager").get[Int]("total")
      participants <- c.downField("ranking").as[List[ParticipantPosition]](decodeParticipantPositions(leagueType))
    } yield
      League(
        leagueId = leagueId,
        leagueType = leagueType,
        participantsCount = total,
        participants = participants
      )
}