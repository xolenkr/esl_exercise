package com.esl.exercise.service

import com.esl.exercise.domain.{League, ParticipantPoints, ParticipantPosition}

object PointDistributionService {

  def distribute(league: League): List[ParticipantPoints] =
    league.participants.map((p: ParticipantPosition) => toParticipantPoints(league.participantsCount, p))

  private def toParticipantPoints(participantsCount: Int, participant: ParticipantPosition): ParticipantPoints =
    ParticipantPoints(
      participantId = participant.participantId,
      points = if (participant.position > 0) participantsCount/participant.position else 0)
}