package com.esl.exercise.service

import com.esl.exercise.domain.ParticipantPoints
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AggregateParticipantsServiceTest extends AnyWordSpec with Matchers {

  trait Fixtures {
    val firstParticipantPoints: ParticipantPoints = ParticipantPoints(
      participantId = "1",
      points = 3.0)
    val secondParticipantPoints: ParticipantPoints = ParticipantPoints(
      participantId = "2",
      points = 5.0)
    val thirdParticipantPoints: ParticipantPoints = ParticipantPoints(
      participantId = "3",
      points = 2.0)
    val firstParticipantExtraPoints: ParticipantPoints = ParticipantPoints(
      participantId = "1",
      points = 5.0)
  }

  "aggregate" should {
    "aggregate points for the same participant" in new Fixtures {
      val participants: List[ParticipantPoints] = List(
        firstParticipantPoints,
        secondParticipantPoints,
        thirdParticipantPoints,
        firstParticipantExtraPoints)

      val result: List[ParticipantPoints] = AggregateParticipantsService.aggregate(participants)

      result shouldBe List(
        firstParticipantPoints.copy(points = 8),
        secondParticipantPoints,
        thirdParticipantPoints
      )
    }
    "work with empty list" in new Fixtures {
      val result: List[ParticipantPoints] = AggregateParticipantsService.aggregate(List.empty)

      result shouldBe List.empty
    }
    "work when nothing to aggregate" in new Fixtures {
      val participants: List[ParticipantPoints] = List(
        firstParticipantPoints,
        secondParticipantPoints,
        thirdParticipantPoints)

      val result: List[ParticipantPoints] = AggregateParticipantsService.aggregate(participants)

      result shouldBe List(
        firstParticipantPoints,
        secondParticipantPoints,
        thirdParticipantPoints
      )
    }
  }
}
