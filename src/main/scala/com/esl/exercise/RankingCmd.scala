package com.esl.exercise

import com.esl.exercise.client.LeagueHttpClient
import com.esl.exercise.domain.Ranking
import com.esl.exercise.service.LeagueService
import com.twitter.util.Await
import org.rogach.scallop.ScallopConf
import io.circe.syntax._
import io.circe.generic.auto._

object RankingCmd {

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    val leagueIds = opt[List[String]](required = true)
    val topX = opt[Int](required = true)
    verify()
  }

  val leagueClient: LeagueHttpClient = LeagueHttpClient()
  val leagueService: LeagueService = new LeagueService(leagueClient)

  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    val result: List[Ranking] = Await.result(leagueService.getRankings(conf.leagueIds(), limit = conf.topX()))
    println("Rankings:")
    println(result.asJson)
  }
}
