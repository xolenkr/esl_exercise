package com.esl.exercise.service

import com.esl.exercise.client.LeagueClient
import com.esl.exercise.domain._
import com.twitter.util.Future

class LeagueService(leagueClient: LeagueClient) {

  def getRankings(leagueIds: List[String], limit: Int): Future[List[Ranking]] =
    Future.traverseSequentially(leagueIds)(leagueId => leagueClient.getLeagueWithParticipants(leagueId, limit))
      .map(leagues => {
        val participantPoints: List[ParticipantPoints] = leagues.flatMap(league => PointDistributionService.distribute(league)).toList
        val aggregatedParticipantPoints = AggregateParticipantsService.aggregate(participantPoints)
        SortParticipantsService.sort(aggregatedParticipantPoints, limit)
      })
}