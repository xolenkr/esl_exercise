package com.esl.exercise.domain

case class Ranking(position: Int, label: String, participantId: String, points: Double)
