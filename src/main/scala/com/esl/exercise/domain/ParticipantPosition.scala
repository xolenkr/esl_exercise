package com.esl.exercise.domain

case class ParticipantPosition(participantId: String, position: Int)
