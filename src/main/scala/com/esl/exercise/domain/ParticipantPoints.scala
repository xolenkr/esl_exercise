package com.esl.exercise.domain

case class ParticipantPoints(participantId: String, points: Double)
