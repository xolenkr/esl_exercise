package com.esl.exercise.service

import com.esl.exercise.domain.ParticipantPoints


object AggregateParticipantsService {

  def aggregate(participants: List[ParticipantPoints]): List[ParticipantPoints] = {
    participants
      .groupBy(_.participantId)
      .map {
        case (participantId, groupedPoints) => ParticipantPoints(
          participantId = participantId,
          points = groupedPoints.map(_.points).sum
        )
      }.toList
  }
}