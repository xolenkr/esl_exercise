# ESL exercise

## Project description
This project is a Framework-based project that does RESTful API calls, aggregating the data and offering it via an API

## Description of usage
This project based on the [FinatraServer](https://twitter.github.io/finatra)

After running application `sbt "runMain com.esl.exercise.FinatraServerMain"` it will be available by the following link:<br>
http://localhost:8080/rankings?leagueIds=90116,92347,134540&top_x=3

Input parameters:
- `leagueIds` list of league IDs (for example 90116,92347,134540)
- `top_x` amount of top contestant (for example 3)

The functionality calls the [ESL API](https://docs.tet.io/#/) to fetch data from the route `GET /leagues/{leagueId}/ranking`
parses JSON, aggregates and calculates the response
 
Example of result below:
```
[
  {
    position: 1,
    label: "1",
    participant_id: "6633867",
    points: 403
  },
  {
    position: 2,
    label: "2",
    participant_id: "9482972",
    points: 201
  },
  {
    position: 3,
    label: "3",
    participant_id: "3111615",
    points: 134
  }
] 
```
    
To run unit tests of the project execute the command `sbt test`                                                                                                                           

## Run via command line

`sbt "runMain com.esl.exercise.RankingCmd --league-ids 202640 134540 92347 --top-x 5"`

## Distributing the points
I didn't use the simple aggregation by interpreting ranks as points and
sort ascending as proposed in the task:
- rank 1 -> 1 point
- rank 2 -> 2 points
- ...

Because in this case participant who won 1st position in the one league always will be on the first place 
and other participants who have for example 2rd, 3nd and others positions in many other leagues 
could never be on the top of the winners.
Imagine I won in `League A`, but never participated in `League B` and `League C`. 
While you were second in `LeagueA`, first in `League B` and first in `League C`. 
According to points allocation that is proposed in task description. I will have `1 point` while you will have `4 points`. 
So I will be always on first place (sort ASC is applied). In my opinion it is not right. 

That's why I used the following points distribution logic:
`participant points = total count of the participants in league / position`.
For example if league has 95 participants then:
- 95/1 -> 95 points for the 1st place
- 95/2 -> 47 points for the 2nd place
- ....
- 95/5 -> 19 points for the 5th place



