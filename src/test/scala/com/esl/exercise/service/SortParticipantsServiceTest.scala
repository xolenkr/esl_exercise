package com.esl.exercise.service

import com.esl.exercise.domain.{ParticipantPoints, Ranking}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SortParticipantsServiceTest extends AnyWordSpec with Matchers {

  trait Fixtures{
    val limit: Int = 3
    val participants: List[ParticipantPoints] = List(
      ParticipantPoints("1", 20),
      ParticipantPoints("2", 45),
      ParticipantPoints("3", 10),
      ParticipantPoints("4", 55),
    )
    val rankings: List[Ranking] = List(
      Ranking(1, "1", "4", 55),
      Ranking(2, "2", "2", 45),
      Ranking(3, "3", "1", 20),
    )
  }

  "sort" should {
    "sort list of ParticipantPoints descending and apply limit" in new Fixtures {
    val result: List[Ranking] = SortParticipantsService.sort(participants, limit)
      result shouldBe rankings
    }
    "work with empty ParticipantPoints list" in new Fixtures {
      val result: List[Ranking] = SortParticipantsService.sort(List.empty, limit)
      result shouldBe List.empty
    }
  }
}
