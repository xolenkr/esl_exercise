package com.esl.exercise

import com.esl.exercise.client.LeagueHttpClient
import com.esl.exercise.controller.RankingController
import com.esl.exercise.service.LeagueService
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.{CommonFilters, LoggingMDCFilter}
import com.twitter.finatra.http.routing.HttpRouter

object FinatraServerMain extends FinatraServer

class FinatraServer extends HttpServer {

  override def defaultHttpPort: String = ":8080"

  val leagueClient: LeagueHttpClient = LeagueHttpClient()
  val leagueService: LeagueService = new LeagueService(leagueClient)
  val rankingController: RankingController = new RankingController(leagueService)

  override def configureHttp(router: HttpRouter): Unit =
    router
      .filter[LoggingMDCFilter[Request, Response]]
      .filter[CommonFilters]
      .add(rankingController)
}
