package com.esl.exercise.service

import com.esl.exercise.domain.{League, ParticipantPoints, ParticipantPosition}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PointDistributionServiceTest extends AnyWordSpec with Matchers {

  trait Fixtures {
    val league: League = League(
      leagueId = "1",
      leagueType = "users",
      participantsCount = 50,
      participants = List(
        ParticipantPosition("1", 1),
        ParticipantPosition("2", 2),
        ParticipantPosition("3", 3)
      )
    )

    val leagueWithZeroPositionParticipant: League = League(
      leagueId = "1",
      leagueType = "users",
      participantsCount = 50,
      participants = List(
        ParticipantPosition("1", 1),
        ParticipantPosition("2", 2),
        ParticipantPosition("3", 0)
      )
    )
  }

  "distribute" should {
    "allocate points for participant" in new Fixtures {
      val result: List[ParticipantPoints] = PointDistributionService.distribute(league)
      result shouldBe List(
        ParticipantPoints("1", 50.0),
        ParticipantPoints("2", 25.0),
        ParticipantPoints("3", 16.0))
    }
    "work with empty league" in new Fixtures {
      val result: List[ParticipantPoints] = PointDistributionService.distribute(league.copy(participants = List.empty))
      result shouldBe List.empty
    }
    "allocate point for participant with 0 position" in new Fixtures {
      val result: List[ParticipantPoints] = PointDistributionService.distribute(leagueWithZeroPositionParticipant)
      result shouldBe List(
        ParticipantPoints("1", 50.0),
        ParticipantPoints("2", 25.0),
        ParticipantPoints("3", 0))
    }
  }
}
