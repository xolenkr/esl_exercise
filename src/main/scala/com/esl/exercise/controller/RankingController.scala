package com.esl.exercise.controller

import com.esl.exercise.controller.RankingController.RankingRequest
import com.esl.exercise.service.LeagueService
import com.twitter.finatra.http.Controller
import com.twitter.finatra.http.annotations.QueryParam

class RankingController(leagueService: LeagueService) extends Controller {

  get("/rankings") { request: RankingRequest =>
    leagueService.getRankings(request.leagueIds, request.limit)
  }
}

object RankingController {
  case class RankingRequest(@QueryParam(value = "leagueIds", commaSeparatedList = true) leagueIds: List[String],
                            @QueryParam("top_x") limit: Int)
}
