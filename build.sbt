name := "esl_exercise"

version := "0.1"

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-parser" % "0.14.2",
  "io.circe" %% "circe-generic" % "0.14.2",
  "com.twitter" %% "finagle-http" % "22.4.0",
  "ch.qos.logback" % "logback-classic" % "1.2.11",
  "org.scalatest" %% "scalatest" % "3.2.12" % "test",
  "com.twitter" %% "finatra-http-server" % "22.4.0",
  "org.rogach" %% "scallop" % "4.1.0"
)